package models;

import org.codehaus.jackson.JsonNode;

import play.mvc.WebSocket;

/**
 * Join message where 
 * username is the name of the user who has joined and 
 * channel is its websocket out channel
 * 
 * @author bnemeth
 *
 */
public class Join {
    
    final String username;
    final WebSocket.Out<JsonNode> channel;
    
    public Join(String username, WebSocket.Out<JsonNode> channel) {
        this.username = username;
        this.channel = channel;
    }
    
}