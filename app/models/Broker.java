package models;

import static akka.pattern.Patterns.ask;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import play.Logger;
import play.libs.Akka;
import play.libs.F.Callback;
import play.libs.F.Callback0;
import play.libs.Json;
import play.mvc.WebSocket;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;

/**
 * A broker is an Actor.
 */
public class Broker extends UntypedActor {
    
    // Default room.
    private static ActorRef defaultRoom = Akka.system().actorOf(new Props(Broker.class));
    
    // Peers
    private Map<String, WebSocket.Out<JsonNode>> members = new HashMap<String, WebSocket.Out<JsonNode>>();
    
    /**
     * Join the default room.
     */
    public static void join(final String username, WebSocket.In<JsonNode> in, WebSocket.Out<JsonNode> out) throws Exception{
        
        // Send the Join message to the room
        String result = (String)Await.result(ask(defaultRoom,new Join(username, out), 1000), Duration.create(1, SECONDS));
        if("OK".equals(result)) {
            
            // For each signal event received on the socket,
            in.onMessage(new Callback<JsonNode>() {
               public void invoke(JsonNode event) {
                   
            	   Logger.debug("Event received: " + event);
                   // Send a Signal message to the room.
                   defaultRoom.tell(new Signal(username, event),defaultRoom);
                   
               } 
            });
            
            // When the socket is closed.
            in.onClose(new Callback0() {
               public void invoke() {
                   
                   // Send a Quit message to the room.
                   defaultRoom.tell(new Quit(username),defaultRoom);
                   
               }
            });
            
        } else {
            
            // Cannot connect, create a Json error.
            ObjectNode error = Json.newObject();
            error.put("error", result);
            
            // Send the error to the socket.
            out.write(error);
            
        }
        
    }
    
    public void onReceive(Object message) throws Exception {
        
        if(message instanceof Join) {
            
            // Received a Join message
            Join join = (Join)message;
            
            // Check if this username is free.
            if(members.containsKey(join.username)) {
            	Logger.debug("This username is already used");
                getSender().tell("This username is already used",defaultRoom);
            } else {
                members.put(join.username, join.channel);
            	Logger.debug(join.username + " has entered the room");
                notifyAll("join", join.username, "has entered the room");
                getSender().tell("OK",defaultRoom);
            }
            
        } else if(message instanceof Quit)  {
            
            // Received a Quit message
            Quit quit = (Quit)message;
            
            members.remove(quit.username);
            
            notifyOthers("quit", quit.username, "has left the room");
        } else if(message instanceof Signal)  {
        	
        	notifyOthers((Signal) message);
        	
        } else {
            unhandled(message);
        }
        
    }
    
    // Send a Json event to all members
    public void notifyAll(String kind, String user, String text) {
        for(WebSocket.Out<JsonNode> channel: members.values()) {
            
            sendMessage(kind, user, text, channel);
        }
    }

    // Send a Signal to the other members
    public void notifyOthers(Signal signal) {
        for(Map.Entry<String, WebSocket.Out<JsonNode>> entry : members.entrySet()) {

        	if(!entry.getKey().equals(signal.username)){
            	entry.getValue().write((signal).jsonNode);
        	}
        }
    }
    
    // Send a Json event to other members
    public void notifyOthers(String kind, String user, String text) {
        for(Map.Entry<String, WebSocket.Out<JsonNode>> entry : members.entrySet()) {

        	if(!entry.getKey().equals(user)){
        		sendMessage(kind, user, text, entry.getValue());
        	}
        }
    }

	private void sendMessage(String kind, String user, String text, WebSocket.Out<JsonNode> channel) {
		
		Logger.debug("About to notify " + user);
		
		ObjectNode event = Json.newObject();
		event.put("type", kind);
		event.put("user", user);
		event.put("message", text);
		
		ArrayNode members = event.putArray("members");
		for(String member: this.members.keySet()) {
		    members.add(member);
		}
		
		channel.write(event);
	}
}
