package models;

/**
 * Quit message
 * username is the name of the user who has just quit
 * @author bnemeth
 *
 */
public class Quit {

	final String username;

	public Quit(String username) {
		this.username = username;
	}

}