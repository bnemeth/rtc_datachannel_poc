package models;

import org.codehaus.jackson.JsonNode;

/**
 * Class representing an ICE message from a user
 * @author bnemeth
 *
 */
public class Signal {

	final String username;
	final JsonNode jsonNode;

	public Signal(String username, JsonNode jsonNode) {
		this.username = username;
		this.jsonNode = jsonNode;
	}

	@Override
	public String toString() {
		return "Signal [username=" + username + ", jsonNode=" + jsonNode + "]";
	}

}
